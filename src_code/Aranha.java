import java.net.SocketException;

/**
 *
 * @author Humberto Miranda
 */
public class Aranha{  
    
    static Agentes aranha = new Agentes("aranha", 500);
    public static void main(String args[]) throws SocketException, InterruptedException{  
        
        System.out.println("Agente Aranha iniciado!");
        new Thread(new AranhaEnvia(aranha)).start();
        new Thread(new AranhaRecebe(aranha)).start();
	
    }

}

