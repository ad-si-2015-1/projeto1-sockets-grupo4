import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
*
* @author Diogo Leal
*/

public class ReceberMensagens implements Runnable  {
	private Agentes agente;
	
	public ReceberMensagens(Agentes agente) {
		// TODO Auto-generated constructor stub
		this.agente = agente; 
	}

	@Override
	public void run() {
		try {
			recebermsg();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void recebermsg() throws Exception {
		int porta = 4567;
		DatagramSocket serverSocket = new DatagramSocket(porta);
		byte[] receiveData = new byte[1024];
		byte[] sendData = new byte[1024];
		while (agente.isLive()) {
			DatagramPacket receivePacket = new DatagramPacket(receiveData,receiveData.length);
			System.out.println("Esperando por datagrama. Atualmente sou " + agente.getStatus());
			serverSocket.receive(receivePacket);
			
			String mensagem = new String(receivePacket.getData());
			mensagem = agente.tratarMensagem(mensagem.trim()); //envia para o metodo que vai verificar a mensagem. Usa trim para retirar os espaçocs em branco
			
			InetAddress IPAddress = receivePacket.getAddress(); //pega o ip do pacote recebido
			//int port = receivePacket.getPort(); //pega a porta do pacote recebido

			sendData = mensagem.getBytes(); //transforma em bytes

			DatagramPacket sendPacket = new DatagramPacket(sendData,sendData.length, IPAddress, porta); //monta o pacote de resposta
			
			serverSocket.send(sendPacket); // envia o pacote
			System.out.println("OK\n"); //printa um ok
			
			receiveData = new byte[1024];; //limpa a cache
			if (agente.getStatus().equals("dead")) { //verifica se o agente em questão não morreu no processo
				agente.setLive(false); //se morreu mudar para false
				System.out.println("Fechando conexão...");//exibe que a conexão vai ser fechada
			}
		}
		serverSocket.close();
		
	}
	
}

