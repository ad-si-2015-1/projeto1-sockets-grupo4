import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Humberto Miranda
 */
public class AranhaRecebe implements Runnable{

    private String IPAgente = null;
    private int portaAgente = 0;
    private String statusAgente = "sem status";
    
    Agentes aranha;
    
    AranhaRecebe(Agentes aranha) {
        this.aranha = aranha;
    }
    
    @Override
    public void run() {
        String mensagem = null;
        InetAddress IPAddress = null;
        int porta = 4567;
        DatagramPacket enviarIPAgente = null;
        DatagramSocket aranhaSocket = null;
        
        try {
            aranhaSocket = new DatagramSocket(porta);
        } catch (SocketException ex) {
            Logger.getLogger(Aranha.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        byte[] dadosRecebidos = null;
        byte[] enviarDados = new byte[1024];
        
        while(true){
            dadosRecebidos = new byte[1024];
            DatagramPacket receivePacket = new DatagramPacket(dadosRecebidos,dadosRecebidos.length);
            //System.out.println("Esperando por datagrama UDP na porta " + porta);
           // System.out.println("Esperando pedido de questionamento na porta " + porta);
            try {
                aranhaSocket.receive(receivePacket);
            } catch (IOException ex) {
                Logger.getLogger(Aranha.class.getName()).log(Level.SEVERE, null, ex);
            }
           
            mensagem = new String(receivePacket.getData());
            //System.out.println(mensagem);
            
            /**
             * Condicional responsável pela verificação da mensagem recebida.
             * Se a mensagem for igual a "eu sou um agente!"), o IP do remetente é
             * armazenado na variável IPAgente. Essa variável será informada quando
             * algum agente perguntar qual o IP do ultimo agente armazenado.
             */
            switch (mensagem.trim()) {
                
                case "caçador":
                    efetivaDadosAgente(receivePacket);
                    break;
                case "questionador":
                    efetivaDadosAgente(receivePacket);
                    //System.out.println("Questionador!");
                    break;
                case "respondedor":
                    efetivaDadosAgente(receivePacket);
                    break;
                case "curador":
                    efetivaDadosAgente(receivePacket);
                    break;
                case "zumbi":
                    efetivaDadosAgente(receivePacket);
                    //System.out.println("Zumbi!");
                    break;
                case "qual o ip do ultimo agente?":
                    try {
                        //Armazena IP de quem fez o pedido
                        IPAddress = receivePacket.getAddress();
                        
                        //Armazena a PORTA de quem fez o pedido
                        porta = receivePacket.getPort();
                        
                        /*
                        * Formato de envio de mensagem para o Agente que requisitou.
                        * 192.168.0.1.;4567;respondedor
                        */
                        if(!getStatusAgente().equals("sem status")){
                            mensagem = getIPAgente()+";"+getStatusAgente();
                            System.out.println(mensagem);

                            enviarIPAgente = new DatagramPacket(mensagem.getBytes(), mensagem.length(), IPAddress , porta);
                            aranhaSocket.send(enviarIPAgente);

                            System.out.println("");
                            System.out.println("-----------------------------------------------");
                            System.out.println("Agente: " + getStatusAgente().trim() + " enviado");
                            System.out.println("IP: "+getIPAgente());
                            System.out.println("-----------------------------------------------");
                            System.out.println("");
                        }else{
                            System.out.println("Sem nenhum IP armazenado");
                        }
                        
                    } catch (IOException ex) {
                        Logger.getLogger(AranhaRecebe.class.getName()).log(Level.SEVERE, null, ex);
                    }   break;
            }
            //System.out.println(mensagem);
            receivePacket = null;
        }
    }
    
    public void efetivaDadosAgente(DatagramPacket receivePacket){
        /**
        * Seta a variável IPAgente com o IP do ultimo Agente que foi solicitado
        * Seta status do ultimo Agente
        * Seta porta de envio do ultimo Agente
        */
        setIPAgente(receivePacket.getAddress().toString());
        setPortaAgente(receivePacket.getPort());
        setStatusAgente(new String(receivePacket.getData()).trim());
        
        System.out.println("");
        System.out.println("-----------------------------------------------");
        System.out.println("Agente: " + getStatusAgente() + " armazenado");
        System.out.println("IP: "+getIPAgente());
        System.out.println("-----------------------------------------------");
        System.out.println("");
    }
    
    public String getIPAgente(){
        return IPAgente;
    }
    
    public void setIPAgente(String IPAgente){
        this.IPAgente = IPAgente;
    }
    
    public void setPortaAgente(int portaAgente){
        this.portaAgente = portaAgente;
    }
    
    public int getPortaAgente(){
        return portaAgente;
    }
    
    public void setStatusAgente(String statusAgente){
        this.statusAgente = statusAgente;
    }  
    
    public String getStatusAgente(){
        return statusAgente;
    }
}


