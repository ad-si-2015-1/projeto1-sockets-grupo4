/**
 *
 * @author Diogo Leal
 */

public class Caçador {
	    
	static Agentes caçador = new Agentes("caçador", 4000);
	public static void main(String[] args) throws Exception {
		//new Thread(t1).start();
		new Thread(new ReceberMensagens(caçador)).start();
		new Thread(new EnviarMensagens(caçador)).start();
	}

}

