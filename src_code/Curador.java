/**
 *
 * @author Diogo Leal
 */

public class Curador {
	    
	static Agentes curador = new Agentes("curador", 3000);
	public static void main(String[] args) throws Exception {
		//new Thread(t1).start();
		new Thread(new ReceberMensagens(curador)).start();
		new Thread(new EnviarMensagens(curador)).start();
	}

}

