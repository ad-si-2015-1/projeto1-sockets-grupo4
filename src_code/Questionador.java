import java.net.*;

/**
*
* @author Diogo Leal
*/

class Questionador {
	public static void main(String args[]) throws Exception {

		int porta = 4567;
				
		DatagramSocket serverSocket = new DatagramSocket(porta);

		byte[] receiveData = new byte[1024];
		byte[] sendData = new byte[1024];
		
		Agentes questionador = new Agentes("questionador", 0);
		
		while (questionador.isLive()) {

			DatagramPacket receivePacket = new DatagramPacket(receiveData,receiveData.length);
			System.out.println("Esperando por datagrama. Atualmente sou " + questionador.getStatus());
			serverSocket.receive(receivePacket);
			
			String mensagem = new String(receivePacket.getData());
			mensagem = questionador.tratarMensagem(mensagem.trim()); //envia para o metodo que vai verificar a mensagem. Usa trim para retirar os espaçocs em branco
			
			InetAddress IPAddress = receivePacket.getAddress(); //pega o ip do pacote recebido
			int port = receivePacket.getPort(); //pega a porta do pacote recebido

			sendData = mensagem.getBytes(); //transforma em bytes

			DatagramPacket sendPacket = new DatagramPacket(sendData,sendData.length, IPAddress, port); //monta o pacote de resposta
			
			serverSocket.send(sendPacket); // envia o pacote
			System.out.println("OK\n"); //printa um ok
			
			receiveData = new byte[1024];; //limpa a cache
			if (questionador.getStatus().equals("dead")) {
				questionador.setLive(false);
				System.out.println("Fechando conexão...");
			}
		}
		serverSocket.close();
		
	}

}

