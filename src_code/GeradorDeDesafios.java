
import java.util.Random;

/**
 *
 * @author Wallacy Soares
 */
public class GeradorDeDesafios {
    public String GeradorDesafios(int quantidadeDeNumeros){
        final int MAXIMO = 100;
        char[] operacao= new char[4];
        String desafio;
        int numero;
        operacao[0]='+';
        operacao[1]='-';
        operacao[2]='*';
        operacao[3]='/';
        
        int cont=0;
        desafio= Integer.toString(SorteiaNumeros(MAXIMO)) + " ";
        while(cont<quantidadeDeNumeros){
            desafio= desafio + operacao[SorteiaNumeros(4)-1] + " " + Integer.toString(SorteiaNumeros(MAXIMO)) + " ";
            cont++;
        }
        
        return desafio;
        
        
    }
    public int SorteiaNumeros(int numeroMaximo){
        Random random = new Random();            
        int numeroSorteado = random.nextInt(numeroMaximo)+1;
        return numeroSorteado;
    }
   
    
}