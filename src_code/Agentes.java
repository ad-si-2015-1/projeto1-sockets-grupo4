/**
 *
 * @author Diogo Leal
 */

public class Agentes {
	String status;
	String agente;
	boolean live;
	int tempo;

	public int getTempo() {
		return tempo;
	}

	public void setTempo(int tempo) {
		this.tempo = tempo;
	}

	public boolean isLive() {
		return live;
	}

	public void setLive(boolean live) {
		this.live = live;
	}

	public String getAgente() {
		return agente;
	}

	public void setAgente(String agente) {
		this.agente = agente;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Agentes (String agente, int tempo) {
		setStatus(agente);
		setAgente(agente);
		setLive(true);
		setTempo(tempo);
	}
	
	public String tratarMensagem(String mensagem) {
		String erro = "Nao sou o tipo que voce entrou em contato";
		if (mensagem.equals("Voce eh um agente?")) {
			mensagem = this.getStatus();
		} else if (this.getStatus().equals("zumbi")) { // caso seja zumbi
			if (mensagem.equals("hu mo bu kai fei di tao")) {
				mensagem = "Fui curado! Thanks";
				curar();
			} else if (mensagem.equals("Headshot!")) {
				mensagem = "Zumbi is dead";
				this.setStatus("dead");
			} else {
				mensagem = erro;
			}
		} else if (this.getStatus().equals("questionador")) { // caso seja questionador
			if (mensagem.equals("Pergunta?")) {
				GeradorDeDesafios gerador = new GeradorDeDesafios();
				mensagem = "Pergunta:" + gerador.GeradorDesafios(3);
			} else if (mensagem.equals("Mordida")) {
				mensagem = "virei zumbi";
				this.setStatus("zumbi");
			} else if (mensagem.contains("resposta")) {
				mensagem = "Reposta correta!";
			} else {
				mensagem = erro;
			}
		} else if (this.getStatus().equals("respondedor")) { // caso seja respondendor
			if (mensagem.contains("Pergunta:")) {
				String teste[] = mensagem.split(":");
				System.out.println("teste1 - " + teste[1]);
				SolucionadorDeDesafios solucao = new SolucionadorDeDesafios();
				String resposta = solucao.SolucionaDesafio(teste[1] + " ");
				mensagem = "A resposta da expressão: " + teste[1] + " é " + resposta;
			} else if (mensagem.equals("Mordida")) {
				mensagem = "virei zumbi";
				this.setStatus("zumbi");
			} else {
				mensagem = erro;
			}
		} else {
			mensagem = erro;
		}
		return mensagem;
	}
	
	public String tratarMensagemAranha(String mensagem) {
		String resposta = "fail";
		if (this.getStatus().equals("zumbi")) { // caso seja zumbi
			if (mensagem.equals("questionador") || mensagem.equals("respondedor")) {
				resposta = "Mordida";
			}
		} else if (this.getStatus().equals("respondedor") && mensagem.equals("respondedor")) { // caso seja respondedor
			resposta = "Pergunta?";
		} else if (this.getStatus().equals("caçador") && mensagem.equals("zumbi")) { // caso seja caçador
			resposta = "Headshot!";
		} else if (this.getStatus().equals("curador") && mensagem.equals("zumbi")) { // caso seja curador
			resposta = "hu mo bu kai fei di tao";
		}
		return resposta;
	}
	
	public void curar() { //verifica qual agente era antigamente para ele reassumir seu status. Caso fosse zumbi nato, ele morre
		if (this.agente.equals("questionador")) {
			this.setStatus("questionador");
		} else if (this.agente.equals("respondedor")) {
			this.setStatus("respondedor");
		} else {
			this.setStatus("dead");
		}
	}
}
