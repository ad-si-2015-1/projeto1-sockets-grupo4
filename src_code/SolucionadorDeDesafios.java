/**
 *
 * @author Wallacy Soares
 */
public class SolucionadorDeDesafios {
    public String SolucionaDesafio(String desafio){

        int posicao=0;
        float total=0;
        int operacao=0;
        String resposta="";
        

        while(posicao<desafio.length()){
            String item= new String();
            while(desafio.charAt(posicao)!=' '){
                item= item + desafio.charAt(posicao);
                posicao++;
            }
            if(item.equals("+"))operacao=0;
            else if(item.equals("-"))operacao=1;
            else if(item.equals("*"))operacao=2;
            else if(item.equals("/"))operacao=3;
            else {
                //Transforma a String inteiro e realiza a operação
                int numero = Integer.parseInt(item);
                if(operacao==0)total=total+numero;
                else if(operacao==1)total=total-numero;
                else if(operacao==2)total=total*numero;
                else if(operacao==3)total=total/numero;
            }
            posicao++;
        }
        resposta = String.valueOf(total);
        return resposta;
    }
    
}
