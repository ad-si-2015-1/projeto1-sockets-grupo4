/**
 *
 * @author Diogo Leal
 */

public class Zumbi {
	    
	static Agentes zumbi = new Agentes("zumbi", 5000);
	public static void main(String[] args) throws Exception {
		//new Thread(t1).start();
		new Thread(new ReceberMensagens(zumbi)).start();
		new Thread(new EnviarMensagens(zumbi)).start();
	}

}

