import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
*
* @author Diogo Leal
*/

public class EnviarMensagens implements Runnable  {
	private Agentes agente;
	
	public EnviarMensagens(Agentes agente) {
		// TODO Auto-generated constructor stub
		this.agente = agente; 
	}

	@Override
	public void run() {
		try {
			enviarMsg();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void enviarMsg() throws Exception {
		while (agente.isLive()) {
				
			String servidor = "192.168.0.6"; //endereço da aranha
			int porta = 4567; //porta da aranha
	
			byte[] sendData = new byte[1024]; //cria o array de bytes para enviar
			byte[] receiveData = new byte[1024]; //cria o array de bytes para receber
			
			InetAddress IPAddress = null;
			try {
				IPAddress = InetAddress.getByName(servidor);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
			String mensagem = "qual o ip do ultimo agente?"; //faz a pergunta para aranha
			sendData = mensagem.getBytes(); //transforma em bytes
			//cria o socket para enviar
			DatagramSocket clientSocket = new DatagramSocket();
			
			//monta o datagrama
			DatagramPacket sendPacket = new DatagramPacket(sendData,sendData.length, IPAddress, porta);
			
			//envia o pacote pelo socket criado
			clientSocket.send(sendPacket);
			
			//cria o pacote para receber a resposta
			DatagramPacket receivePacket = new DatagramPacket(receiveData,receiveData.length);
			//aguarda a resposta
			clientSocket.receive(receivePacket);
			System.out.println("Resposta da Aranha recebida...");
	
			String resposta = new String(receivePacket.getData());
                        String respostaaranha[] = resposta.split(";");  
                        System.out.println(respostaaranha[0]); //imprime ip
                        System.out.println(respostaaranha[1]); //imprime status
			mensagem = agente.tratarMensagemAranha(respostaaranha[1]); //envia o status
			System.out.println(mensagem);//printa ação, caso fail não vai fazer nada
                        
                        if(mensagem.equals("fail")){
                        }else{
                            sendData = mensagem.getBytes();
                            IPAddress = InetAddress.getByName(respostaaranha[0]);
                            sendPacket = new DatagramPacket(sendData,sendData.length, IPAddress, porta);
                            clientSocket.send(sendPacket);
                        }
                        
			clientSocket.close();
			System.out.println("Socket cliente fechado!");
			
			//dando um sleep de 5 segundos na thread para ela não ficar toda hora na aranha
			try {
				Thread.sleep(agente.getTempo());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
				
	}
	
}