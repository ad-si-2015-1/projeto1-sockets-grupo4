import java.net.*;
/**
*
* @author Diogo Leal
*/

class UDPClient {
	public static void main(String a4rgs[]) throws Exception {

		DatagramSocket clientSocket = new DatagramSocket();

		String servidor = "localhost";
		int porta = 4568;

		InetAddress IPAddress = InetAddress.getByName(servidor);

		byte[] sendData = new byte[1024];
		byte[] receiveData = new byte[1024];

		//String mensagem = "Mordida"; //zumbi
		//String mensagem = "Pergunta?"; //respondendor
		//String mensagem = "A resposta é 15"; //respondendor
		//String mensagem = "Voce eh um agente"; //aranha
		//String mensagem = "Raio curandeiro!"; //curador
		String mensagem = "Headshot!"; //caçador
		sendData = mensagem.getBytes();
		DatagramPacket sendPacket = new DatagramPacket(sendData,sendData.length, IPAddress, porta);

		System.out.println("Enviando pacote UDP para " + servidor + ":" + porta);
		clientSocket.send(sendPacket);

		DatagramPacket receivePacket = new DatagramPacket(receiveData,receiveData.length);

		clientSocket.receive(receivePacket);
		System.out.println("Pacote UDP recebido...");

		String modifiedSentence = new String(receivePacket.getData());

		System.out.println("Texto recebido do servidor:" + modifiedSentence);
		clientSocket.close();
		System.out.println("Socket cliente fechado!");
	}
}
