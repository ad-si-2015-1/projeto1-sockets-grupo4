/**
 *
 * @author Diogo Leal
 */

public class Respondedor {
	    
	static Agentes respondedor = new Agentes("respondendor", 2000);
	public static void main(String[] args) throws Exception {
		//new Thread(t1).start();
		new Thread(new ReceberMensagens(respondedor)).start();
		new Thread(new EnviarMensagens(respondedor)).start();
	}

}
