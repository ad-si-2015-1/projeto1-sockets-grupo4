import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Humberto Miranda
 */
public class AranhaEnvia implements Runnable{

    static DatagramSocket aranhaSocket = null;
    Agentes aranha;
    
    AranhaEnvia(Agentes aranha) {
        this.aranha = aranha;
    }

    @Override
    public void run() {
        int porta = 4567;
        String subrede = "";
        InetAddress IPAddress = null;
        
        Scanner entrada = new Scanner (System.in);
        System.out.print("Digite o endereço IP da Aranha: ");
        String ipAranha = entrada.nextLine();
        System.out.println("----------------------------------------");
        
        int numPontos=0;  
        
        for(int i=0;i<ipAranha.length();i++){  
            
            subrede = subrede+ipAranha.charAt(i);
            //System.out.println(subrede);
            if(ipAranha.charAt(i)=='.'){
                numPontos++;
            }
            if(numPontos==3){
                break;
            }
        } 
        
        try {
            aranhaSocket = new DatagramSocket();
        } catch (SocketException ex) {
            Logger.getLogger(AranhaEnvia.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DatagramPacket enviarPergunta;

        byte[] enviaDados = new byte[1024];
        String perguntaAoIP = "Voce eh um agente?";
     
        int ipBusca = 0;
        
        Random r = new Random();
        do{
            ipBusca = r.nextInt(255);
            System.out.println("Busca será iniciada no IP: "+subrede+ipBusca);
        }while(ipBusca<1 || ipBusca>255);
        
        
        
        
        
        while(ipBusca <= 255){
            try{
                IPAddress = InetAddress.getByName(subrede+ipBusca);
                enviarPergunta = new DatagramPacket(perguntaAoIP.getBytes(), perguntaAoIP.length(), IPAddress , porta);
                
                if(!IPAddress.equals(InetAddress.getByName(ipAranha))){
                    try {
                        aranhaSocket.send(enviarPergunta);
                    } catch (IOException ex) {
                        Logger.getLogger(AranhaEnvia.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }catch (UnknownHostException ex){
                Logger.getLogger(AranhaEnvia.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            ipBusca++;
            if(ipBusca==256){
                ipBusca=1;
            }
            
            try {   
               Thread.currentThread().sleep(aranha.getTempo()); // meio segundo de delay 
            } catch (InterruptedException ex) {
                Logger.getLogger(AranhaEnvia.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
