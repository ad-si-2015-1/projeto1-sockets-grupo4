### FLUXO DE DESENVOLVIMENTO DO GRUPO 4 NO EMBIENTE GIT - PROJETO 1
----

#### Branch Master


Na branch master conterá 2 arquivos(COLABORADORES.md e README.md) e 2 diretórios(docs e src_code);

* Arquivo COLABORADORES.md -> Deve conter o nome e email de todos os participantes do grupo;
* Arquivo README.md -> Descrição breve do Projeto 1
* Diretório docs -> Terá toda a documentação do projeto(artefatos de documentação, arquivos de imagem, arquivos pdf);
* Diretório src_code -> Terá apenas arquivos JAVA(arquivo.java);


#### Branch NomeParticipante

* Cada participante do grupo terá uma Branch com seu nome, sendo um clone da Branch Master;
* Na Branch NomeParticipante o participante irá desenvolver todo os seus artefatos(documentação ou codificação) seguindo as descrições da Branch Master;

*OBS: NENHUM ARQUIVO PODERÁ SER COMITADO NA RAIZ DA BRANCH APENAS NOS DIRETÓRIOS docs e src_code EXCETO ALTERAÇÃO DOS ARQUIVOS README.MD e COLABORADORES.MD SEGUINDO DESCRIÇÃO DA BRANCH MASTER;*


#### Branch Projeto1


* Branch que sofrerá Merge Request das branchs dos particpantes;
* Após a validação dos MRs o participante deverá fazer outro MR para a Branch Master;

*TODA A CONTABILIZAÇÃO DOS MRs SERÁ FEITA COM BASE NA BRANCH MASTER;*

# ![fluxo_desenvolvimento](imagens/img_fluxo_desenvolvimento.jpg)