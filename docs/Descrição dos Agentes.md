Proposta: Desenvolver uma aplica��o que consiste em um servidor e v�rios clientes, sendo que cada um desses, � um agente inteligente. A responsabilidade do servidor � de executar um dos agentes do projeto, que � o agente ARANHA, e as demais fun��es os clientes assumem. 

S�o as fun��es: 
- Aranha
- Zumbi
- Ca�ador
- Questionador 
- Respondedor
- Curador

A fun��o da Aranha � varrer a rede em busca de outros agentes, identificando seu n�mero IP. Quando questionada, fornece o �ltimo agente localizado. � imune aos zumbis.

O Zumbi, como o nome diz, transforma o agente que entra em contato com ele em um outro zumbi, tornando-se imune aos desafios aritm�ticos.

Ca�ador, esse � o salvador da p�tria, sua fun��o � prender os Zumbis, logo, � imune aos mesmos.

O Questionador tem o trabalho de enviar aos outros agentes, desafios aritm�ticos e avaliar a resposta dos desafios propostos por ele.

O Respondedor, claro, responde desafios aritm�ticos do Questionador. 

Curador, eis aqui outro salvador da p�tria, que por sua vez, cura os Zumbis! Yaaa!!! Logo, � imune aos Zumbis.

Lembrando que o �nico agente capaz de localizar e classificar os outros � a Aranha e essa � imortal.

